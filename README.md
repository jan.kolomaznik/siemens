# PYTHON – Siemens

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Programujete v Pythonu a rádi byste své schopnosti uplatnili při tvorbě automatizovaných testů? 
Obliba programovacího jazyka Python pro automatizaci a skriptování neustále roste. 
Softwarové firmy začínají Python vnímat jako univerzální jazyk pro tvorbu testů pro software napsaný v libovolném programovacím jazyce. 
Přihlaste se na praktický workshop, ve kterém si vyzkoušíte tvorbu automatizovaných testů pro software napsaný nejen v Pythonu, ale v libovolném programovacím jazyce. 
Vyzkoušejte si nástroje pro testování všeho možného od vašeho vlastního kódu přes API až po webové aplikace.


- [**Organizace projektu**](01-python.module.ipynb)
    1. Moduly a balíčky
    2. [Distribuce software](pdm.ipynb)
- **Generátory a iterátory**
    1. Sekvenční datové typy
    2. [Generované sekvence](02-python.function.genarators.ipynb)
    3. [Iterátory](03-python.function.itertools.ipynb)
    4. [Nástroje pro práci s funkcemi](04-python.function.functools.ipynb)
    3. [Čtení textových souborů](05-python.file.ipynb)
- [**Ukládání a zpracování dat**](06-python.save.ipynb)
    1. Ukládání objektů
    2. Datové formáty
- [**Ladění a logování**](07-python.logging.ipynb)
    1. Ladění pomocí výpisů
    2. Standardní logovací knihovna
    3. Debuggery
    4. Používání assertů ve vlastním kódu
- [**Psaní bezpečného kódu**](08-python.save.code.ipynb)
    1. Porovnávání hodnot
    2. Typová kontrola
    3. Okrajové případy
- [**Paralelní zpracování**](09-python.parallelization.ipynb)
    1. [Práce s vlákna](10-python.parallelization.multihreading.ipynb)
    2. [Práce s procesy](11-python.parallelization.multiprocessing.ipynb)
- [**Základy testování**](12-test.ipynb)
    1. Kvalita software
    2. Granularita testů
    3. Rozsah testování
    4. Testování a automatizace
    5. [Nástoje dotuplné v pythonu](13-test.python.ipynb)
- [**Testovací framework (pytest)**](14-test.pytest.ipynb)
    1. Příprava testovacích funkcí
    2. Volby příkazové řádky
    3. Hlavní výhody frameworku
    4. [Pluginy rošiřující framework](15-test.pytest.plugins.ipynb)
- [**Testování kódu**](16-test.code.ipynb)
    1. Jednotkové testy
    2. Testování modulů, funkcí a tříd
    3. Celé aplikace k testování
- [**Psaní testovatelného kódu**](17-test.principles.ipynb)
- [**Negativní testování**](18-test.exceptions.ipynb)
    1. Testování chybných vstupů
    2. Očekávání výjimek
- **Testování aplikací přes API**
    1. [Mnohdy v jiném jazyce](19-test.application.ipynb)
    2. [Destování databází](20-test.db.ipynb)
    2. [Komunikace a vzdálené volání](21-test.api.ipynb)
    3. Testovací instance software
    4. Práce s neznámým stavem
- [**Testování webových a GUI aplikací**](22-test.ui.ipynb)
    1. Testování webových dotazů
    2. Interaktivní testování
    3. Integrace prohlížeče
    4. Možnosti testování GUI
- [**Testování síťové komunikací**](23-python.networking.ipynb)
    1. Práce se TCP/IP spojení
    2. Sledování provozu síťové komunikace