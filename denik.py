# %%
def load():
    from json import load

    with open('journal.json', mode='r') as data:
        journal = load(data)

    return journal

#journal = load()
# %%
from math import sqrt

def phi(n00: int, n01: int, n10: int, n11: int) -> float:
    return ((n11*n00 - n10*n01) / 
            sqrt(
                (n11+n10)*
                (n01+n00)*
                (n01+n11)*
                (n10+n00)
            ))


phi(76, 9, 4, 1)  # 0.068599434


# %%
from typing import Any, Union


def tableFor(event: str|int, journal: list[dict[str, Any]]) -> list[int]:
    """fuction caltulate 4 values desribe variont evnets.

    Args:
        event (_type_): _description_
        journal (_type_): _description_

    Returns:
        _type_: _description_
    """
    
    table = [0,0,0,0]
    for record in journal:
        if event in record['events'] and record['squirrel']:
            table[3] += 1
        if event in record['events'] and not record['squirrel']:
            table[1] += 1
        if event not in record['events'] and record['squirrel']:
            table[2] += 1
        if event not in record['events'] and not record['squirrel']:
            table[0] += 1
    return table
            
#tableFor('pizza', journal)

# %%
if __name__ == '__main__':
    phis = {
        event: phi(*tableFor(event, journal))
        for record in journal for event in record['events']
    }

    sorted(phis.items(), key=lambda x: x[1])

# %%