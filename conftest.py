import pytest
#import requests

import sys

ALL = set("darwin linux win32".split())


def pytest_runtest_setup(item):
    supported_platforms = ALL.intersection(
        mark.name for mark in item.iter_markers())
    plat = sys.platform
    if supported_platforms and plat not in supported_platforms:
        pytest.skip("cannot run on platform {}".format(plat))


@pytest.fixture
def example_people_data_from_conftest():
    return [
        {
            "given_name": "Alfonsa",
            "family_name": "Ruiz",
            "title": "Senior Software Engineer",
        },
        {
            "given_name": "Sayid",
            "family_name": "Khan",
            "title": "Project Manager",
        },
    ]

@pytest.fixture(autouse=True)
def disable_network_calls(monkeypatch):
    import requests
    
    def stunted_get():
        raise RuntimeError("Network access not allowed during testing!")
    
    monkeypatch.setattr(requests, "get", lambda *args, **kwargs: stunted_get())